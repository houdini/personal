import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Score{
	public static void main(String[] args) throws IOException{
		//从命令行中获取后两个文件在当前项目文件夹中的路径
		String pathSm =Score.class.getResource(args[0]).getPath();
		String pathAl =Score.class.getResource(args[1]).getPath();
		String pathTo =Score.class.getResource("total.properties").getPath();
		//使用前面获取到的路径来定位文件，再使用Jsoup对这两个文件进行解析，并合并成一个Elements，方便计算
		Document file = Jsoup.parse(new File(pathSm),"UTF-8");
		Document file1 = Jsoup.parse(new File(pathAl),"UTF-8");
		Elements exps = file.getElementsByClass("interaction-row");
		exps.addAll(file1.getElementsByClass("interaction-row"));
		Properties all = new Properties();
		all.load(new FileInputStream(pathTo));
		
		double examSc = 0;
		double seExSc = 0;
		double baseSc = 0;
		double proSc = 0;
		double plusSc = 0;
		//通过contains()方法区别不同类型的题目来分别获得经验值
		for(Element p:exps) {
			String type = p.text();
			if(type.contains("编程题")) {
				proSc += getSc(p);
			}
			else if(type.contains("附加题")) {
				plusSc += getSc(p);
			}
			else if(type.contains("课前自测")) {
				seExSc += getSc(p);
			}
			else if(type.contains("课堂完成")) {
				baseSc += getSc(p);	
			}
			else if(type.contains("课堂小测")) {
				examSc += getSc(p);
			}
		}
		
		//通过要求的公式进行计算总分
		examSc = (examSc/Double.parseDouble(all.getProperty("test")))*100*0.2;
		seExSc = (seExSc/Double.parseDouble(all.getProperty("before")))*100*0.25;
		baseSc = (baseSc/Double.parseDouble(all.getProperty("base")))*100*0.3*0.95;
		proSc = (proSc/Double.parseDouble(all.getProperty("program")))*95*0.1;
		plusSc = (plusSc/Double.parseDouble(all.getProperty("add")))*90*0.05;
		double allSc = examSc+seExSc+baseSc+proSc+plusSc;
		System.out.printf("%.2f", allSc);
	}

	private static double getSc(Element row) {
		//通过正则表达式对div块内容进行筛选，获得经验值和互评经验值
		double sc = 0;
        String score = row.text();
        Pattern number = Pattern.compile("(\\d+) 经验");
        Matcher matcher = number.matcher(score);
        while (matcher.find()) {
            sc += Double.parseDouble(matcher.group(1));
        }
		return sc;
	}
}